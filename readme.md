# Pilgrim Framework

This framework is for adding to Wordpress themes in order to enhance functionality in the ways that I am familiar with. Instead of creating a single starter theme - as I like to use different themes to try new techniques and for different purpose (such as Bones, Underscores, JointsWP) - I decided to create a drop-in Framework.

## Files

pilgrim_library.php - is a collection of functions that I might always add to WordPress and don't come with every theme. If they do they can be easily removed. Nothing is dependent in Pilgrim Framework.

pilgrim_login.css - a stylesheet called for use just on the login page, gives a much better look to the login page

pilgrim_woocommerce.css - I use woocommerce a lot, so a lot of the main functions and gists that I have picked up will be collected in here. Again they can be commented or deleted or refactored on a per-theme basis. Or just not called at all if not using WooCommerce.

### Usage

Simple add:

<?php require get_template_directory() . '/pigrim-framework/pilgrim_library.php'; ?>

to the functions file of the theme you are working with and drop Pilgrim-Framework into the theme directory.