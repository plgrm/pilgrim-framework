<?php
/**
 * Pilgrim Library
 *
 * This is where I include the functions that I want with every 
 * Wordpress theme I create, and where I can include other 
 * functions that I might need for this particular project.
 * Thanks to Eddie Machado's Bones for a lot of this good stuff.
 *
 * @package pilgrim
 * @since 1.0
 * @version 1.0
 */
 
/************* ADD PILGRIM STYLES *****************/

// wp_enqueue_style( 'pilgrim-main-style', get_template_directory_uri().'/pilgrim/css/styles.css' );
 
/************* DASHBOARD WIDGETS *****************/

function disable_default_dashboard_widgets() {
	
	 /* Remove Dashboard Widgets
	  *
	  * Remove default and useless dashboard widgets
	  *
	  * @package pilgrim
	  * @since 1.0
	  * @version 1.0
	  */
	
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // Right Now Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']); // Activity Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // Incoming Links Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // Plugins Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // Quick Press Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // Recent Drafts Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); //
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); //
	
	// Remove common plugin dashboard widgets
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']); // Yoast's SEO Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']); // Gravity Forms Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']); // bbPress Plugin Widget
}

function pilgrim_rss_dashboard_widget() {
	
	 /* Create Pilgrim RSS Admin Widget
	  *
	  * Add the Pilgrim Blog RSS feed to the admin backend
	  * developed from Eddie Machado's Bones theme http://www.themble.com/bones
	  *
	  * @package pilgrim
	  * @since 1.0
	  * @version 1.0
	  */
	
	if ( function_exists( 'fetch_feed' ) ) {
		// include_once( ABSPATH . WPINC . '/feed.php' );               // include the required file
		$feed = fetch_feed( 'http://plgrm.co/feed/rss' );        // specify the source feed
		if (is_wp_error($feed)) {
			$limit = 0;
			$items = 0;
		} else {
			$limit = $feed->get_item_quantity(7);                        // specify number of items
			$items = $feed->get_items(0, $limit);                        // create an array of items
		}
	}
	if ($limit == 0) echo '<div>The RSS Feed is either empty or unavailable.</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo mysql2date( __( 'j F Y @ g:i a', 'pilgrim' ), $item->get_date( 'Y-m-d H:i:s' ) ); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>
	<?php }
		
}

function pilgrim_custom_dashboard_widgets() {
	
	 /* Call Extra Dashboard Widgets
	  *
	  * @package pilgrim
	  * @since 1.0
	  * @version 1.0
	  *
	  */
	  
	  wp_add_dashboard_widget( 'pilgrim_rss_dashboard_widget', __( 'From the Pilgrim blog', 'pilgrim' ), 'pilgrim_rss_dashboard_widget' );
	  
}

/* Add these actions to the Wordpress dashboard */

	add_action( 'wp_dashboard_setup', 'pilgrim_custom_dashboard_widgets' );
	add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );

/************* CUSTOM LOGIN PAGE *****************/

/* Add a custom login.css file to the login page */
	function pilgrim_login_css() { wp_enqueue_style( 'pilgrim_login_css', get_template_directory_uri() . '/pilgrim/pilgrim_login.css', false ); }

/* Change the logo link to the home page of the blog */
	function pilgrim_login_url() { return home_url(); }

/* Change the alt text of the logo to the blog title */
	function pilgrim_login_title() { return get_option( 'blogname' ); }

/* Call these only for the login page */
	add_action( 'login_enqueue_scripts', 'pilgrim_login_css', 10 );
	add_filter( 'login_headerurl', 'pilgrim_login_url' );
	add_filter( 'login_headertitle', 'pilgrim_login_title' );

/************* CUSTOMIZE ADMIN FOOTER *******************/

function pilgrim_custom_admin_footer() { _e( '<span id="footer-thankyou">a website by <a href="http://plgrm.co" target="_blank">Pilgrim</a></span>', 'pilgrim' );
}
add_filter( 'admin_footer_text', 'pilgrim_custom_admin_footer' );
 
/************* THUMBNAIL SIZE OPTIONS *************/

/* Add thumbnail image sizes */

	add_image_size( 'pilgrim-thumb-600', 600, 150, true );
	add_image_size( 'pilgrim-thumb-300', 300, 100, true );

/*
*
*	To use the function:
*	<?php the_post_thumbnail( 'pilgrim-thumb-300' ); ?>
*
*/

	add_filter( 'image_size_names_choose', 'pilgrim_custom_image_sizes' );

function pilgrim_custom_image_sizes( $sizes ) {
	
	 /* Add Sizes to Media Manager
	  *
	  * @package pilgrim
	  * @since 1.0
	  * @version 1.0
	  *
	  */
	
    return array_merge( $sizes, array(
        'pilgrim-thumb-600' => __('600px by 150px'),
        'pilgrim-thumb-300' => __('300px by 100px'),
    ) );
}
